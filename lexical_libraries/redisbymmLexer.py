# Generated from redisbymm.g4 by ANTLR 4.5.2
from antlr4 import *
from io import StringIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\21")
        buf.write("n\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\3\2\3\2\3\2\3\2\3\2\3\3\3\3")
        buf.write("\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3")
        buf.write("\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b")
        buf.write("\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3")
        buf.write("\13\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3")
        buf.write("\f\3\r\3\r\3\r\3\r\3\r\3\16\6\16d\n\16\r\16\16\16e\3\17")
        buf.write("\6\17i\n\17\r\17\16\17j\3\20\3\20\2\2\21\3\3\5\4\7\5\t")
        buf.write("\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20")
        buf.write("\37\21\3\2\4\3\2\62;\4\2C\\c|o\2\3\3\2\2\2\2\5\3\2\2\2")
        buf.write("\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17")
        buf.write("\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3")
        buf.write("\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2")
        buf.write("\2\2\3!\3\2\2\2\5&\3\2\2\2\7+\3\2\2\2\t\60\3\2\2\2\13")
        buf.write("\65\3\2\2\2\r:\3\2\2\2\17?\3\2\2\2\21G\3\2\2\2\23L\3\2")
        buf.write("\2\2\25P\3\2\2\2\27U\3\2\2\2\31]\3\2\2\2\33c\3\2\2\2\35")
        buf.write('h\3\2\2\2\37l\3\2\2\2!"\7U\2\2"#\7C\2\2#$\7F\2\2$%\7')
        buf.write("F\2\2%\4\3\2\2\2&'\7U\2\2'(\7F\2\2()\7G\2\2)*\7N\2\2")
        buf.write("*\6\3\2\2\2+,\7U\2\2,-\7I\2\2-.\7G\2\2./\7V\2\2/\b\3\2")
        buf.write("\2\2\60\61\7N\2\2\61\62\7C\2\2\62\63\7F\2\2\63\64\7F\2")
        buf.write("\2\64\n\3\2\2\2\65\66\7N\2\2\66\67\7F\2\2\678\7G\2\28")
        buf.write("9\7N\2\29\f\3\2\2\2:;\7N\2\2;<\7I\2\2<=\7G\2\2=>\7V\2")
        buf.write("\2>\16\3\2\2\2?@\7N\2\2@A\7I\2\2AB\7G\2\2BC\7V\2\2CD\7")
        buf.write("C\2\2DE\7N\2\2EF\7N\2\2F\20\3\2\2\2GH\7N\2\2HI\7R\2\2")
        buf.write("IJ\7Q\2\2JK\7R\2\2K\22\3\2\2\2LM\7I\2\2MN\7G\2\2NO\7V")
        buf.write("\2\2O\24\3\2\2\2PQ\7U\2\2QR\7G\2\2RS\7V\2\2ST\7X\2\2T")
        buf.write("\26\3\2\2\2UV\7I\2\2VW\7G\2\2WX\7V\2\2XY\7V\2\2YZ\7[\2")
        buf.write("\2Z[\7R\2\2[\\\7G\2\2\\\30\3\2\2\2]^\7F\2\2^_\7G\2\2_")
        buf.write("`\7N\2\2`a\7M\2\2a\32\3\2\2\2bd\t\2\2\2cb\3\2\2\2de\3")
        buf.write('\2\2\2ec\3\2\2\2ef\3\2\2\2f\34\3\2\2\2gi\7"\2\2hg\3\2')
        buf.write("\2\2ij\3\2\2\2jh\3\2\2\2jk\3\2\2\2k\36\3\2\2\2lm\t\3\2")
        buf.write("\2m \3\2\2\2\5\2ej\2")
        return buf.getvalue()


class redisbymmLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [DFA(ds, i) for i, ds in enumerate(atn.decisionToState)]

    SADD_KEY = 1
    SDEL_KEY = 2
    SGET_KEY = 3
    LADD_KEY = 4
    LDEL_KEY = 5
    LGET_KEY = 6
    LGETALL_KEY = 7
    LPOP_KEY = 8
    GET_KEY = 9
    SET_KEY = 10
    TYPE_KEY = 11
    DELK_KEY = 12
    DIGIT = 13
    SPACE = 14
    LETTER = 15

    modeNames = ["DEFAULT_MODE"]

    literalNames = [
        "<INVALID>",
        "'SADD'",
        "'SDEL'",
        "'SGET'",
        "'LADD'",
        "'LDEL'",
        "'LGET'",
        "'LGETALL'",
        "'LPOP'",
        "'GET'",
        "'SETV'",
        "'GETTYPE'",
        "'DELK'",
    ]

    symbolicNames = [
        "<INVALID>",
        "SADD_KEY",
        "SDEL_KEY",
        "SGET_KEY",
        "LADD_KEY",
        "LDEL_KEY",
        "LGET_KEY",
        "LGETALL_KEY",
        "LPOP_KEY",
        "GET_KEY",
        "SET_KEY",
        "TYPE_KEY",
        "DELK_KEY",
        "DIGIT",
        "SPACE",
        "LETTER",
    ]

    ruleNames = [
        "SADD_KEY",
        "SDEL_KEY",
        "SGET_KEY",
        "LADD_KEY",
        "LDEL_KEY",
        "LGET_KEY",
        "LGETALL_KEY",
        "LPOP_KEY",
        "GET_KEY",
        "SET_KEY",
        "TYPE_KEY",
        "DELK_KEY",
        "DIGIT",
        "SPACE",
        "LETTER",
    ]

    grammarFileName = "redisbymm.g4"

    def __init__(self, input=None):
        super().__init__(input)
        self.checkVersion("4.5.2")
        self._interp = LexerATNSimulator(
            self, self.atn, self.decisionsToDFA, PredictionContextCache()
        )
        self._actions = None
        self._predicates = None
