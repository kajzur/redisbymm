grammar redisbymm;

expression
    : (sadd|sdel|sget|ladd|ldel|lget|lgetall|lpop|get|setv|gettype|delk)
    ;
sadd
    : SADD_KEY SPACE key SPACE string_index SPACE val
    ;
SADD_KEY
    : 'SADD'
    ;
sdel
    : SDEL_KEY SPACE key SPACE string_index
    ;
SDEL_KEY
    : 'SDEL'
    ;
sget
    : SGET_KEY SPACE key SPACE string_index
    ;
SGET_KEY
    : 'SGET'
    ;
ladd
    : LADD_KEY SPACE key SPACE val (SPACE val)* 
    ;
LADD_KEY
    : 'LADD'
    ;
ldel
    : LDEL_KEY SPACE key SPACE index (SPACE index)* 
    ;
LDEL_KEY
    : 'LDEL'
    ;
lget
    : LGET_KEY SPACE key SPACE index
    ;
LGET_KEY:
    'LGET'
    ;
lgetall
    : LGETALL_KEY SPACE key
    ;
LGETALL_KEY
    : 'LGETALL'
    ;
lpop
    : LPOP_KEY SPACE key SPACE index
    ;
LPOP_KEY
    : 'LPOP'
    ;
get
    : GET_KEY SPACE key
    ;
GET_KEY
    : 'GET'
    ;
setv
    : SET_KEY SPACE key SPACE val (SPACE key SPACE val)*
    ;
SET_KEY
    : 'SETV'
    ;
gettype
    : TYPE_KEY SPACE key
    ;
TYPE_KEY
    : 'GETTYPE'
    ;
delk
    : DELK_KEY SPACE key
    ;
DELK_KEY
    : 'DELK'
    ;
index
    : DIGIT+
    ;
DIGIT
    : [0-9]+
    ;
SPACE
    : ' '+
    ;
key
    : (LETTER)+(DIGIT)*
    ;
val
    : (LETTER)*(DIGIT)*
    ;
string_index
    : (LETTER)+(DIGIT)*
    ;
LETTER
    : [a-zA-Z]
    ;
