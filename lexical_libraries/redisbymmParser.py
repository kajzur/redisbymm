# Generated from redisbymm.g4 by ANTLR 4.5.2
# encoding: utf-8
from antlr4 import *
from io import StringIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\21")
        buf.write("\u00b1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\3\2\3\2")
        buf.write("\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\5\2\61\n\2\3")
        buf.write("\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4")
        buf.write("\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\7")
        buf.write("\6N\n\6\f\6\16\6Q\13\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\7\7")
        buf.write("Z\n\7\f\7\16\7]\13\7\3\b\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3")
        buf.write("\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\f")
        buf.write("\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\7\f}\n\f\f\f\16\f")
        buf.write("\u0080\13\f\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\17\6")
        buf.write("\17\u008b\n\17\r\17\16\17\u008c\3\20\6\20\u0090\n\20\r")
        buf.write("\20\16\20\u0091\3\20\7\20\u0095\n\20\f\20\16\20\u0098")
        buf.write("\13\20\3\21\7\21\u009b\n\21\f\21\16\21\u009e\13\21\3\21")
        buf.write("\7\21\u00a1\n\21\f\21\16\21\u00a4\13\21\3\22\6\22\u00a7")
        buf.write("\n\22\r\22\16\22\u00a8\3\22\7\22\u00ac\n\22\f\22\16\22")
        buf.write("\u00af\13\22\3\22\2\2\23\2\4\6\b\n\f\16\20\22\24\26\30")
        buf.write('\32\34\36 "\2\2\u00b4\2\60\3\2\2\2\4\62\3\2\2\2\6:\3')
        buf.write("\2\2\2\b@\3\2\2\2\nF\3\2\2\2\fR\3\2\2\2\16^\3\2\2\2\20")
        buf.write("d\3\2\2\2\22h\3\2\2\2\24n\3\2\2\2\26r\3\2\2\2\30\u0081")
        buf.write("\3\2\2\2\32\u0085\3\2\2\2\34\u008a\3\2\2\2\36\u008f\3")
        buf.write('\2\2\2 \u009c\3\2\2\2"\u00a6\3\2\2\2$\61\5\4\3\2%\61')
        buf.write("\5\6\4\2&\61\5\b\5\2'\61\5\n\6\2(\61\5\f\7\2)\61\5\16")
        buf.write("\b\2*\61\5\20\t\2+\61\5\22\n\2,\61\5\24\13\2-\61\5\26")
        buf.write("\f\2.\61\5\30\r\2/\61\5\32\16\2\60$\3\2\2\2\60%\3\2\2")
        buf.write("\2\60&\3\2\2\2\60'\3\2\2\2\60(\3\2\2\2\60)\3\2\2\2\60")
        buf.write("*\3\2\2\2\60+\3\2\2\2\60,\3\2\2\2\60-\3\2\2\2\60.\3\2")
        buf.write("\2\2\60/\3\2\2\2\61\3\3\2\2\2\62\63\7\3\2\2\63\64\7\20")
        buf.write('\2\2\64\65\5\36\20\2\65\66\7\20\2\2\66\67\5"\22\2\67')
        buf.write("8\7\20\2\289\5 \21\29\5\3\2\2\2:;\7\4\2\2;<\7\20\2\2<")
        buf.write('=\5\36\20\2=>\7\20\2\2>?\5"\22\2?\7\3\2\2\2@A\7\5\2\2')
        buf.write('AB\7\20\2\2BC\5\36\20\2CD\7\20\2\2DE\5"\22\2E\t\3\2\2')
        buf.write("\2FG\7\6\2\2GH\7\20\2\2HI\5\36\20\2IJ\7\20\2\2JO\5 \21")
        buf.write("\2KL\7\20\2\2LN\5 \21\2MK\3\2\2\2NQ\3\2\2\2OM\3\2\2\2")
        buf.write("OP\3\2\2\2P\13\3\2\2\2QO\3\2\2\2RS\7\7\2\2ST\7\20\2\2")
        buf.write("TU\5\36\20\2UV\7\20\2\2V[\5\34\17\2WX\7\20\2\2XZ\5\34")
        buf.write("\17\2YW\3\2\2\2Z]\3\2\2\2[Y\3\2\2\2[\\\3\2\2\2\\\r\3\2")
        buf.write("\2\2][\3\2\2\2^_\7\b\2\2_`\7\20\2\2`a\5\36\20\2ab\7\20")
        buf.write("\2\2bc\5\34\17\2c\17\3\2\2\2de\7\t\2\2ef\7\20\2\2fg\5")
        buf.write("\36\20\2g\21\3\2\2\2hi\7\n\2\2ij\7\20\2\2jk\5\36\20\2")
        buf.write("kl\7\20\2\2lm\5\34\17\2m\23\3\2\2\2no\7\13\2\2op\7\20")
        buf.write("\2\2pq\5\36\20\2q\25\3\2\2\2rs\7\f\2\2st\7\20\2\2tu\5")
        buf.write("\36\20\2uv\7\20\2\2v~\5 \21\2wx\7\20\2\2xy\5\36\20\2y")
        buf.write("z\7\20\2\2z{\5 \21\2{}\3\2\2\2|w\3\2\2\2}\u0080\3\2\2")
        buf.write("\2~|\3\2\2\2~\177\3\2\2\2\177\27\3\2\2\2\u0080~\3\2\2")
        buf.write("\2\u0081\u0082\7\r\2\2\u0082\u0083\7\20\2\2\u0083\u0084")
        buf.write("\5\36\20\2\u0084\31\3\2\2\2\u0085\u0086\7\16\2\2\u0086")
        buf.write("\u0087\7\20\2\2\u0087\u0088\5\36\20\2\u0088\33\3\2\2\2")
        buf.write("\u0089\u008b\7\17\2\2\u008a\u0089\3\2\2\2\u008b\u008c")
        buf.write("\3\2\2\2\u008c\u008a\3\2\2\2\u008c\u008d\3\2\2\2\u008d")
        buf.write("\35\3\2\2\2\u008e\u0090\7\21\2\2\u008f\u008e\3\2\2\2\u0090")
        buf.write("\u0091\3\2\2\2\u0091\u008f\3\2\2\2\u0091\u0092\3\2\2\2")
        buf.write("\u0092\u0096\3\2\2\2\u0093\u0095\7\17\2\2\u0094\u0093")
        buf.write("\3\2\2\2\u0095\u0098\3\2\2\2\u0096\u0094\3\2\2\2\u0096")
        buf.write("\u0097\3\2\2\2\u0097\37\3\2\2\2\u0098\u0096\3\2\2\2\u0099")
        buf.write("\u009b\7\21\2\2\u009a\u0099\3\2\2\2\u009b\u009e\3\2\2")
        buf.write("\2\u009c\u009a\3\2\2\2\u009c\u009d\3\2\2\2\u009d\u00a2")
        buf.write("\3\2\2\2\u009e\u009c\3\2\2\2\u009f\u00a1\7\17\2\2\u00a0")
        buf.write("\u009f\3\2\2\2\u00a1\u00a4\3\2\2\2\u00a2\u00a0\3\2\2\2")
        buf.write("\u00a2\u00a3\3\2\2\2\u00a3!\3\2\2\2\u00a4\u00a2\3\2\2")
        buf.write("\2\u00a5\u00a7\7\21\2\2\u00a6\u00a5\3\2\2\2\u00a7\u00a8")
        buf.write("\3\2\2\2\u00a8\u00a6\3\2\2\2\u00a8\u00a9\3\2\2\2\u00a9")
        buf.write("\u00ad\3\2\2\2\u00aa\u00ac\7\17\2\2\u00ab\u00aa\3\2\2")
        buf.write("\2\u00ac\u00af\3\2\2\2\u00ad\u00ab\3\2\2\2\u00ad\u00ae")
        buf.write("\3\2\2\2\u00ae#\3\2\2\2\u00af\u00ad\3\2\2\2\r\60O[~\u008c")
        buf.write("\u0091\u0096\u009c\u00a2\u00a8\u00ad")
        return buf.getvalue()


class redisbymmParser(Parser):

    grammarFileName = "redisbymm.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [DFA(ds, i) for i, ds in enumerate(atn.decisionToState)]

    sharedContextCache = PredictionContextCache()

    literalNames = [
        "<INVALID>",
        "'SADD'",
        "'SDEL'",
        "'SGET'",
        "'LADD'",
        "'LDEL'",
        "'LGET'",
        "'LGETALL'",
        "'LPOP'",
        "'GET'",
        "'SETV'",
        "'GETTYPE'",
        "'DELK'",
    ]

    symbolicNames = [
        "<INVALID>",
        "SADD_KEY",
        "SDEL_KEY",
        "SGET_KEY",
        "LADD_KEY",
        "LDEL_KEY",
        "LGET_KEY",
        "LGETALL_KEY",
        "LPOP_KEY",
        "GET_KEY",
        "SET_KEY",
        "TYPE_KEY",
        "DELK_KEY",
        "DIGIT",
        "SPACE",
        "LETTER",
    ]

    RULE_expression = 0
    RULE_sadd = 1
    RULE_sdel = 2
    RULE_sget = 3
    RULE_ladd = 4
    RULE_ldel = 5
    RULE_lget = 6
    RULE_lgetall = 7
    RULE_lpop = 8
    RULE_get = 9
    RULE_setv = 10
    RULE_gettype = 11
    RULE_delk = 12
    RULE_index = 13
    RULE_key = 14
    RULE_val = 15
    RULE_string_index = 16

    ruleNames = [
        "expression",
        "sadd",
        "sdel",
        "sget",
        "ladd",
        "ldel",
        "lget",
        "lgetall",
        "lpop",
        "get",
        "setv",
        "gettype",
        "delk",
        "index",
        "key",
        "val",
        "string_index",
    ]

    EOF = Token.EOF
    SADD_KEY = 1
    SDEL_KEY = 2
    SGET_KEY = 3
    LADD_KEY = 4
    LDEL_KEY = 5
    LGET_KEY = 6
    LGETALL_KEY = 7
    LPOP_KEY = 8
    GET_KEY = 9
    SET_KEY = 10
    TYPE_KEY = 11
    DELK_KEY = 12
    DIGIT = 13
    SPACE = 14
    LETTER = 15

    def __init__(self, input: TokenStream):
        super().__init__(input)
        self.checkVersion("4.5.2")
        self._interp = ParserATNSimulator(
            self, self.atn, self.decisionsToDFA, self.sharedContextCache
        )
        self._predicates = None

    class ExpressionContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def sadd(self):
            return self.getTypedRuleContext(redisbymmParser.SaddContext, 0)

        def sdel(self):
            return self.getTypedRuleContext(redisbymmParser.SdelContext, 0)

        def sget(self):
            return self.getTypedRuleContext(redisbymmParser.SgetContext, 0)

        def ladd(self):
            return self.getTypedRuleContext(redisbymmParser.LaddContext, 0)

        def ldel(self):
            return self.getTypedRuleContext(redisbymmParser.LdelContext, 0)

        def lget(self):
            return self.getTypedRuleContext(redisbymmParser.LgetContext, 0)

        def lgetall(self):
            return self.getTypedRuleContext(redisbymmParser.LgetallContext, 0)

        def lpop(self):
            return self.getTypedRuleContext(redisbymmParser.LpopContext, 0)

        def get(self):
            return self.getTypedRuleContext(redisbymmParser.GetContext, 0)

        def setv(self):
            return self.getTypedRuleContext(redisbymmParser.SetvContext, 0)

        def gettype(self):
            return self.getTypedRuleContext(redisbymmParser.GettypeContext, 0)

        def delk(self):
            return self.getTypedRuleContext(redisbymmParser.DelkContext, 0)

        def getRuleIndex(self):
            return redisbymmParser.RULE_expression

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterExpression"):
                listener.enterExpression(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitExpression"):
                listener.exitExpression(self)

    def expression(self):

        localctx = redisbymmParser.ExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_expression)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 46
            token = self._input.LA(1)
            if token in [redisbymmParser.SADD_KEY]:
                self.state = 34
                self.sadd()

            elif token in [redisbymmParser.SDEL_KEY]:
                self.state = 35
                self.sdel()

            elif token in [redisbymmParser.SGET_KEY]:
                self.state = 36
                self.sget()

            elif token in [redisbymmParser.LADD_KEY]:
                self.state = 37
                self.ladd()

            elif token in [redisbymmParser.LDEL_KEY]:
                self.state = 38
                self.ldel()

            elif token in [redisbymmParser.LGET_KEY]:
                self.state = 39
                self.lget()

            elif token in [redisbymmParser.LGETALL_KEY]:
                self.state = 40
                self.lgetall()

            elif token in [redisbymmParser.LPOP_KEY]:
                self.state = 41
                self.lpop()

            elif token in [redisbymmParser.GET_KEY]:
                self.state = 42
                self.get()

            elif token in [redisbymmParser.SET_KEY]:
                self.state = 43
                self.setv()

            elif token in [redisbymmParser.TYPE_KEY]:
                self.state = 44
                self.gettype()

            elif token in [redisbymmParser.DELK_KEY]:
                self.state = 45
                self.delk()

            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SaddContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SADD_KEY(self):
            return self.getToken(redisbymmParser.SADD_KEY, 0)

        def SPACE(self, i: int = None):
            if i is None:
                return self.getTokens(redisbymmParser.SPACE)
            else:
                return self.getToken(redisbymmParser.SPACE, i)

        def key(self):
            return self.getTypedRuleContext(redisbymmParser.KeyContext, 0)

        def string_index(self):
            return self.getTypedRuleContext(redisbymmParser.String_indexContext, 0)

        def val(self):
            return self.getTypedRuleContext(redisbymmParser.ValContext, 0)

        def getRuleIndex(self):
            return redisbymmParser.RULE_sadd

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterSadd"):
                listener.enterSadd(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitSadd"):
                listener.exitSadd(self)

    def sadd(self):

        localctx = redisbymmParser.SaddContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_sadd)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 48
            self.match(redisbymmParser.SADD_KEY)
            self.state = 49
            self.match(redisbymmParser.SPACE)
            self.state = 50
            self.key()
            self.state = 51
            self.match(redisbymmParser.SPACE)
            self.state = 52
            self.string_index()
            self.state = 53
            self.match(redisbymmParser.SPACE)
            self.state = 54
            self.val()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SdelContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SDEL_KEY(self):
            return self.getToken(redisbymmParser.SDEL_KEY, 0)

        def SPACE(self, i: int = None):
            if i is None:
                return self.getTokens(redisbymmParser.SPACE)
            else:
                return self.getToken(redisbymmParser.SPACE, i)

        def key(self):
            return self.getTypedRuleContext(redisbymmParser.KeyContext, 0)

        def string_index(self):
            return self.getTypedRuleContext(redisbymmParser.String_indexContext, 0)

        def getRuleIndex(self):
            return redisbymmParser.RULE_sdel

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterSdel"):
                listener.enterSdel(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitSdel"):
                listener.exitSdel(self)

    def sdel(self):

        localctx = redisbymmParser.SdelContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_sdel)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 56
            self.match(redisbymmParser.SDEL_KEY)
            self.state = 57
            self.match(redisbymmParser.SPACE)
            self.state = 58
            self.key()
            self.state = 59
            self.match(redisbymmParser.SPACE)
            self.state = 60
            self.string_index()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SgetContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SGET_KEY(self):
            return self.getToken(redisbymmParser.SGET_KEY, 0)

        def SPACE(self, i: int = None):
            if i is None:
                return self.getTokens(redisbymmParser.SPACE)
            else:
                return self.getToken(redisbymmParser.SPACE, i)

        def key(self):
            return self.getTypedRuleContext(redisbymmParser.KeyContext, 0)

        def string_index(self):
            return self.getTypedRuleContext(redisbymmParser.String_indexContext, 0)

        def getRuleIndex(self):
            return redisbymmParser.RULE_sget

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterSget"):
                listener.enterSget(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitSget"):
                listener.exitSget(self)

    def sget(self):

        localctx = redisbymmParser.SgetContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_sget)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 62
            self.match(redisbymmParser.SGET_KEY)
            self.state = 63
            self.match(redisbymmParser.SPACE)
            self.state = 64
            self.key()
            self.state = 65
            self.match(redisbymmParser.SPACE)
            self.state = 66
            self.string_index()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class LaddContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LADD_KEY(self):
            return self.getToken(redisbymmParser.LADD_KEY, 0)

        def SPACE(self, i: int = None):
            if i is None:
                return self.getTokens(redisbymmParser.SPACE)
            else:
                return self.getToken(redisbymmParser.SPACE, i)

        def key(self):
            return self.getTypedRuleContext(redisbymmParser.KeyContext, 0)

        def val(self, i: int = None):
            if i is None:
                return self.getTypedRuleContexts(redisbymmParser.ValContext)
            else:
                return self.getTypedRuleContext(redisbymmParser.ValContext, i)

        def getRuleIndex(self):
            return redisbymmParser.RULE_ladd

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterLadd"):
                listener.enterLadd(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitLadd"):
                listener.exitLadd(self)

    def ladd(self):

        localctx = redisbymmParser.LaddContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_ladd)
        self._la = 0  # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 68
            self.match(redisbymmParser.LADD_KEY)
            self.state = 69
            self.match(redisbymmParser.SPACE)
            self.state = 70
            self.key()
            self.state = 71
            self.match(redisbymmParser.SPACE)
            self.state = 72
            self.val()
            self.state = 77
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la == redisbymmParser.SPACE:
                self.state = 73
                self.match(redisbymmParser.SPACE)
                self.state = 74
                self.val()
                self.state = 79
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class LdelContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LDEL_KEY(self):
            return self.getToken(redisbymmParser.LDEL_KEY, 0)

        def SPACE(self, i: int = None):
            if i is None:
                return self.getTokens(redisbymmParser.SPACE)
            else:
                return self.getToken(redisbymmParser.SPACE, i)

        def key(self):
            return self.getTypedRuleContext(redisbymmParser.KeyContext, 0)

        def index(self, i: int = None):
            if i is None:
                return self.getTypedRuleContexts(redisbymmParser.IndexContext)
            else:
                return self.getTypedRuleContext(redisbymmParser.IndexContext, i)

        def getRuleIndex(self):
            return redisbymmParser.RULE_ldel

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterLdel"):
                listener.enterLdel(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitLdel"):
                listener.exitLdel(self)

    def ldel(self):

        localctx = redisbymmParser.LdelContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_ldel)
        self._la = 0  # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 80
            self.match(redisbymmParser.LDEL_KEY)
            self.state = 81
            self.match(redisbymmParser.SPACE)
            self.state = 82
            self.key()
            self.state = 83
            self.match(redisbymmParser.SPACE)
            self.state = 84
            self.index()
            self.state = 89
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la == redisbymmParser.SPACE:
                self.state = 85
                self.match(redisbymmParser.SPACE)
                self.state = 86
                self.index()
                self.state = 91
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class LgetContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LGET_KEY(self):
            return self.getToken(redisbymmParser.LGET_KEY, 0)

        def SPACE(self, i: int = None):
            if i is None:
                return self.getTokens(redisbymmParser.SPACE)
            else:
                return self.getToken(redisbymmParser.SPACE, i)

        def key(self):
            return self.getTypedRuleContext(redisbymmParser.KeyContext, 0)

        def index(self):
            return self.getTypedRuleContext(redisbymmParser.IndexContext, 0)

        def getRuleIndex(self):
            return redisbymmParser.RULE_lget

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterLget"):
                listener.enterLget(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitLget"):
                listener.exitLget(self)

    def lget(self):

        localctx = redisbymmParser.LgetContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_lget)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 92
            self.match(redisbymmParser.LGET_KEY)
            self.state = 93
            self.match(redisbymmParser.SPACE)
            self.state = 94
            self.key()
            self.state = 95
            self.match(redisbymmParser.SPACE)
            self.state = 96
            self.index()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class LgetallContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LGETALL_KEY(self):
            return self.getToken(redisbymmParser.LGETALL_KEY, 0)

        def SPACE(self):
            return self.getToken(redisbymmParser.SPACE, 0)

        def key(self):
            return self.getTypedRuleContext(redisbymmParser.KeyContext, 0)

        def getRuleIndex(self):
            return redisbymmParser.RULE_lgetall

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterLgetall"):
                listener.enterLgetall(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitLgetall"):
                listener.exitLgetall(self)

    def lgetall(self):

        localctx = redisbymmParser.LgetallContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_lgetall)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 98
            self.match(redisbymmParser.LGETALL_KEY)
            self.state = 99
            self.match(redisbymmParser.SPACE)
            self.state = 100
            self.key()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class LpopContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LPOP_KEY(self):
            return self.getToken(redisbymmParser.LPOP_KEY, 0)

        def SPACE(self, i: int = None):
            if i is None:
                return self.getTokens(redisbymmParser.SPACE)
            else:
                return self.getToken(redisbymmParser.SPACE, i)

        def key(self):
            return self.getTypedRuleContext(redisbymmParser.KeyContext, 0)

        def index(self):
            return self.getTypedRuleContext(redisbymmParser.IndexContext, 0)

        def getRuleIndex(self):
            return redisbymmParser.RULE_lpop

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterLpop"):
                listener.enterLpop(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitLpop"):
                listener.exitLpop(self)

    def lpop(self):

        localctx = redisbymmParser.LpopContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_lpop)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 102
            self.match(redisbymmParser.LPOP_KEY)
            self.state = 103
            self.match(redisbymmParser.SPACE)
            self.state = 104
            self.key()
            self.state = 105
            self.match(redisbymmParser.SPACE)
            self.state = 106
            self.index()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class GetContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def GET_KEY(self):
            return self.getToken(redisbymmParser.GET_KEY, 0)

        def SPACE(self):
            return self.getToken(redisbymmParser.SPACE, 0)

        def key(self):
            return self.getTypedRuleContext(redisbymmParser.KeyContext, 0)

        def getRuleIndex(self):
            return redisbymmParser.RULE_get

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterGet"):
                listener.enterGet(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitGet"):
                listener.exitGet(self)

    def get(self):

        localctx = redisbymmParser.GetContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_get)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 108
            self.match(redisbymmParser.GET_KEY)
            self.state = 109
            self.match(redisbymmParser.SPACE)
            self.state = 110
            self.key()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SetvContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SET_KEY(self):
            return self.getToken(redisbymmParser.SET_KEY, 0)

        def SPACE(self, i: int = None):
            if i is None:
                return self.getTokens(redisbymmParser.SPACE)
            else:
                return self.getToken(redisbymmParser.SPACE, i)

        def key(self, i: int = None):
            if i is None:
                return self.getTypedRuleContexts(redisbymmParser.KeyContext)
            else:
                return self.getTypedRuleContext(redisbymmParser.KeyContext, i)

        def val(self, i: int = None):
            if i is None:
                return self.getTypedRuleContexts(redisbymmParser.ValContext)
            else:
                return self.getTypedRuleContext(redisbymmParser.ValContext, i)

        def getRuleIndex(self):
            return redisbymmParser.RULE_setv

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterSetv"):
                listener.enterSetv(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitSetv"):
                listener.exitSetv(self)

    def setv(self):

        localctx = redisbymmParser.SetvContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_setv)
        self._la = 0  # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 112
            self.match(redisbymmParser.SET_KEY)
            self.state = 113
            self.match(redisbymmParser.SPACE)
            self.state = 114
            self.key()
            self.state = 115
            self.match(redisbymmParser.SPACE)
            self.state = 116
            self.val()
            self.state = 124
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la == redisbymmParser.SPACE:
                self.state = 117
                self.match(redisbymmParser.SPACE)
                self.state = 118
                self.key()
                self.state = 119
                self.match(redisbymmParser.SPACE)
                self.state = 120
                self.val()
                self.state = 126
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class GettypeContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def TYPE_KEY(self):
            return self.getToken(redisbymmParser.TYPE_KEY, 0)

        def SPACE(self):
            return self.getToken(redisbymmParser.SPACE, 0)

        def key(self):
            return self.getTypedRuleContext(redisbymmParser.KeyContext, 0)

        def getRuleIndex(self):
            return redisbymmParser.RULE_gettype

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterGettype"):
                listener.enterGettype(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitGettype"):
                listener.exitGettype(self)

    def gettype(self):

        localctx = redisbymmParser.GettypeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_gettype)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 127
            self.match(redisbymmParser.TYPE_KEY)
            self.state = 128
            self.match(redisbymmParser.SPACE)
            self.state = 129
            self.key()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class DelkContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def DELK_KEY(self):
            return self.getToken(redisbymmParser.DELK_KEY, 0)

        def SPACE(self):
            return self.getToken(redisbymmParser.SPACE, 0)

        def key(self):
            return self.getTypedRuleContext(redisbymmParser.KeyContext, 0)

        def getRuleIndex(self):
            return redisbymmParser.RULE_delk

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterDelk"):
                listener.enterDelk(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitDelk"):
                listener.exitDelk(self)

    def delk(self):

        localctx = redisbymmParser.DelkContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_delk)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 131
            self.match(redisbymmParser.DELK_KEY)
            self.state = 132
            self.match(redisbymmParser.SPACE)
            self.state = 133
            self.key()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class IndexContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def DIGIT(self, i: int = None):
            if i is None:
                return self.getTokens(redisbymmParser.DIGIT)
            else:
                return self.getToken(redisbymmParser.DIGIT, i)

        def getRuleIndex(self):
            return redisbymmParser.RULE_index

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterIndex"):
                listener.enterIndex(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitIndex"):
                listener.exitIndex(self)

    def index(self):

        localctx = redisbymmParser.IndexContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_index)
        self._la = 0  # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 136
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 135
                self.match(redisbymmParser.DIGIT)
                self.state = 138
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la == redisbymmParser.DIGIT):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class KeyContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LETTER(self, i: int = None):
            if i is None:
                return self.getTokens(redisbymmParser.LETTER)
            else:
                return self.getToken(redisbymmParser.LETTER, i)

        def DIGIT(self, i: int = None):
            if i is None:
                return self.getTokens(redisbymmParser.DIGIT)
            else:
                return self.getToken(redisbymmParser.DIGIT, i)

        def getRuleIndex(self):
            return redisbymmParser.RULE_key

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterKey"):
                listener.enterKey(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitKey"):
                listener.exitKey(self)

    def key(self):

        localctx = redisbymmParser.KeyContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_key)
        self._la = 0  # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 141
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 140
                self.match(redisbymmParser.LETTER)
                self.state = 143
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la == redisbymmParser.LETTER):
                    break

            self.state = 148
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la == redisbymmParser.DIGIT:
                self.state = 145
                self.match(redisbymmParser.DIGIT)
                self.state = 150
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ValContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LETTER(self, i: int = None):
            if i is None:
                return self.getTokens(redisbymmParser.LETTER)
            else:
                return self.getToken(redisbymmParser.LETTER, i)

        def DIGIT(self, i: int = None):
            if i is None:
                return self.getTokens(redisbymmParser.DIGIT)
            else:
                return self.getToken(redisbymmParser.DIGIT, i)

        def getRuleIndex(self):
            return redisbymmParser.RULE_val

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterVal"):
                listener.enterVal(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitVal"):
                listener.exitVal(self)

    def val(self):

        localctx = redisbymmParser.ValContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_val)
        self._la = 0  # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 154
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la == redisbymmParser.LETTER:
                self.state = 151
                self.match(redisbymmParser.LETTER)
                self.state = 156
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 160
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la == redisbymmParser.DIGIT:
                self.state = 157
                self.match(redisbymmParser.DIGIT)
                self.state = 162
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class String_indexContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LETTER(self, i: int = None):
            if i is None:
                return self.getTokens(redisbymmParser.LETTER)
            else:
                return self.getToken(redisbymmParser.LETTER, i)

        def DIGIT(self, i: int = None):
            if i is None:
                return self.getTokens(redisbymmParser.DIGIT)
            else:
                return self.getToken(redisbymmParser.DIGIT, i)

        def getRuleIndex(self):
            return redisbymmParser.RULE_string_index

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterString_index"):
                listener.enterString_index(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitString_index"):
                listener.exitString_index(self)

    def string_index(self):

        localctx = redisbymmParser.String_indexContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_string_index)
        self._la = 0  # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 164
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 163
                self.match(redisbymmParser.LETTER)
                self.state = 166
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la == redisbymmParser.LETTER):
                    break

            self.state = 171
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la == redisbymmParser.DIGIT:
                self.state = 168
                self.match(redisbymmParser.DIGIT)
                self.state = 173
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx
