from module_factory import ModuleFactory
from lexical_libraries.redisbymmParser import redisbymmParser
from lexical_libraries.redisbymmListener import redisbymmListener
from module_factory.modules import LaddModule
from antlr4.error.ErrorListener import ErrorListener
from database.named_semaphore import named_semaphore


class RedisByMMListener(redisbymmListener):
    def __init__(self, socket):
        self.socket = socket
        self.current_module = None

    def enterGet(self, ctx: redisbymmParser.GetContext):
        self.current_module = ModuleFactory.create_for_token("GET")

    def exitGet(self, ctx: redisbymmParser.GetContext):
        result = self.current_module.run()
        self.socket.send(result)

    def enterSetv(self, ctx: redisbymmParser.GetContext):
        self.current_module = ModuleFactory.create_for_token("SETV")

    def exitSetv(self, ctx: redisbymmParser.SetvContext):
        result = self.current_module.run()
        self.socket.send(result)

    def enterGettype(self, ctx: redisbymmParser.GettypeContext):
        self.current_module = ModuleFactory.create_for_token("GETTYPE")

    def exitGettype(self, ctx: redisbymmParser.GettypeContext):
        result = self.current_module.run()
        self.socket.send(result)

    def enterSadd(self, ctx: redisbymmParser.SaddContext):
        self.current_module = ModuleFactory.create_for_token("SADD")

    def exitSadd(self, ctx: redisbymmParser.SaddContext):
        result = self.current_module.run()
        self.socket.send(result)

    def enterSdel(self, ctx: redisbymmParser.SdelContext):
        self.current_module = ModuleFactory.create_for_token("SDEL")

    def exitSdel(self, ctx: redisbymmParser.SdelContext):
        result = self.current_module.run()
        self.socket.send(result)

    def enterSget(self, ctx: redisbymmParser.SgetContext):
        self.current_module = ModuleFactory.create_for_token("SGET")

    def exitSget(self, ctx: redisbymmParser.SgetContext):
        result = self.current_module.run()
        self.socket.send(result)

    def enterLadd(self, ctx: redisbymmParser.LaddContext):
        self.current_module = ModuleFactory.create_for_token("LADD")

    def exitLadd(self, ctx: redisbymmParser.LaddContext):
        result = self.current_module.run()
        self.socket.send(result)

    def enterLdel(self, ctx: redisbymmParser.LdelContext):
        self.current_module = ModuleFactory.create_for_token("LDEL")

    def exitLdel(self, ctx: redisbymmParser.LdelContext):
        result = self.current_module.run()
        self.socket.send(result)

    def enterLget(self, ctx: redisbymmParser.LgetContext):
        self.current_module = ModuleFactory.create_for_token("LGET")

    def exitLget(self, ctx: redisbymmParser.LgetContext):
        result = self.current_module.run()
        self.socket.send(result)

    def enterLgetall(self, ctx: redisbymmParser.LgetallContext):
        self.current_module = ModuleFactory.create_for_token("LGETALL")

    def exitLgetall(self, ctx: redisbymmParser.LgetallContext):
        result = self.current_module.run()
        self.socket.send(result)

    def enterLpop(self, ctx: redisbymmParser.LpopContext):
        self.current_module = ModuleFactory.create_for_token("LPOP")

    def exitLpop(self, ctx: redisbymmParser.LpopContext):
        result = self.current_module.run()
        self.socket.send(result)

    def enterDelk(self, ctx: redisbymmParser.DelkContext):
        self.current_module = ModuleFactory.create_for_token("DELK")

    def exitDelk(self, ctx: redisbymmParser.DelkContext):
        result = self.current_module.run()
        self.socket.send(result)

    def enterIndex(self, ctx: redisbymmParser.IndexContext):
        self.current_module.set_index(ctx.getText())

    def exitKey(self, ctx: redisbymmParser.KeyContext):
        self.current_module.set_key(ctx.getText())

    def exitVal(self, ctx: redisbymmParser.ValContext):
        self.current_module.set_value(
            ctx.getText(), create=isinstance(self.current_module, LaddModule)
        )

    def exitString_index(self, ctx: redisbymmParser.String_indexContext):
        self.current_module.set_string_index(ctx.getText())


class RedisByMMErrorListener(ErrorListener):
    def __init__(self, socket):
        self.socket = socket
        self.not_important_msgs = ["no viable alternative at input '<EOF>'"]

    def syntaxError(self, recognizer, offendingSymbol, line, column, msg, e):
        named_semaphore.release_all()
        error_msg = "line " + str(line) + ":" + str(column) + " " + msg
        if msg not in self.not_important_msgs:
            self.socket.send(str(error_msg).encode())
