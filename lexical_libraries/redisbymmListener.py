# Generated from redisbymm.g4 by ANTLR 4.5.2
from antlr4 import *

if __name__ is not None and "." in __name__:
    from .redisbymmParser import redisbymmParser
else:
    from redisbymmParser import redisbymmParser

# This class defines a complete listener for a parse tree produced by redisbymmParser.
class redisbymmListener(ParseTreeListener):

    # Enter a parse tree produced by redisbymmParser#expression.
    def enterExpression(self, ctx: redisbymmParser.ExpressionContext):
        pass

    # Exit a parse tree produced by redisbymmParser#expression.
    def exitExpression(self, ctx: redisbymmParser.ExpressionContext):
        pass

    # Enter a parse tree produced by redisbymmParser#sadd.
    def enterSadd(self, ctx: redisbymmParser.SaddContext):
        pass

    # Exit a parse tree produced by redisbymmParser#sadd.
    def exitSadd(self, ctx: redisbymmParser.SaddContext):
        pass

    # Enter a parse tree produced by redisbymmParser#sdel.
    def enterSdel(self, ctx: redisbymmParser.SdelContext):
        pass

    # Exit a parse tree produced by redisbymmParser#sdel.
    def exitSdel(self, ctx: redisbymmParser.SdelContext):
        pass

    # Enter a parse tree produced by redisbymmParser#sget.
    def enterSget(self, ctx: redisbymmParser.SgetContext):
        pass

    # Exit a parse tree produced by redisbymmParser#sget.
    def exitSget(self, ctx: redisbymmParser.SgetContext):
        pass

    # Enter a parse tree produced by redisbymmParser#ladd.
    def enterLadd(self, ctx: redisbymmParser.LaddContext):
        pass

    # Exit a parse tree produced by redisbymmParser#ladd.
    def exitLadd(self, ctx: redisbymmParser.LaddContext):
        pass

    # Enter a parse tree produced by redisbymmParser#ldel.
    def enterLdel(self, ctx: redisbymmParser.LdelContext):
        pass

    # Exit a parse tree produced by redisbymmParser#ldel.
    def exitLdel(self, ctx: redisbymmParser.LdelContext):
        pass

    # Enter a parse tree produced by redisbymmParser#lget.
    def enterLget(self, ctx: redisbymmParser.LgetContext):
        pass

    # Exit a parse tree produced by redisbymmParser#lget.
    def exitLget(self, ctx: redisbymmParser.LgetContext):
        pass

    # Enter a parse tree produced by redisbymmParser#lgetall.
    def enterLgetall(self, ctx: redisbymmParser.LgetallContext):
        pass

    # Exit a parse tree produced by redisbymmParser#lgetall.
    def exitLgetall(self, ctx: redisbymmParser.LgetallContext):
        pass

    # Enter a parse tree produced by redisbymmParser#lpop.
    def enterLpop(self, ctx: redisbymmParser.LpopContext):
        pass

    # Exit a parse tree produced by redisbymmParser#lpop.
    def exitLpop(self, ctx: redisbymmParser.LpopContext):
        pass

    # Enter a parse tree produced by redisbymmParser#get.
    def enterGet(self, ctx: redisbymmParser.GetContext):
        pass

    # Exit a parse tree produced by redisbymmParser#get.
    def exitGet(self, ctx: redisbymmParser.GetContext):
        pass

    # Enter a parse tree produced by redisbymmParser#setv.
    def enterSetv(self, ctx: redisbymmParser.SetvContext):
        pass

    # Exit a parse tree produced by redisbymmParser#setv.
    def exitSetv(self, ctx: redisbymmParser.SetvContext):
        pass

    # Enter a parse tree produced by redisbymmParser#gettype.
    def enterGettype(self, ctx: redisbymmParser.GettypeContext):
        pass

    # Exit a parse tree produced by redisbymmParser#gettype.
    def exitGettype(self, ctx: redisbymmParser.GettypeContext):
        pass

    # Enter a parse tree produced by redisbymmParser#delk.
    def enterDelk(self, ctx: redisbymmParser.DelkContext):
        pass

    # Exit a parse tree produced by redisbymmParser#delk.
    def exitDelk(self, ctx: redisbymmParser.DelkContext):
        pass

    # Enter a parse tree produced by redisbymmParser#index.
    def enterIndex(self, ctx: redisbymmParser.IndexContext):
        pass

    # Exit a parse tree produced by redisbymmParser#index.
    def exitIndex(self, ctx: redisbymmParser.IndexContext):
        pass

    # Enter a parse tree produced by redisbymmParser#key.
    def enterKey(self, ctx: redisbymmParser.KeyContext):
        pass

    # Exit a parse tree produced by redisbymmParser#key.
    def exitKey(self, ctx: redisbymmParser.KeyContext):
        pass

    # Enter a parse tree produced by redisbymmParser#val.
    def enterVal(self, ctx: redisbymmParser.ValContext):
        pass

    # Exit a parse tree produced by redisbymmParser#val.
    def exitVal(self, ctx: redisbymmParser.ValContext):
        pass

    # Enter a parse tree produced by redisbymmParser#string_index.
    def enterString_index(self, ctx: redisbymmParser.String_indexContext):
        pass

    # Exit a parse tree produced by redisbymmParser#string_index.
    def exitString_index(self, ctx: redisbymmParser.String_indexContext):
        pass
