#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

requirements = ["antlr4-python3-runtime==4.5.2", "gevent==1.2.2"]

setup(
    name="redisbymm",
    version="0.0.1",
    packages=find_packages("."),
    include_package_data=True,
    entry_points={"console_scripts": ["redisbymm = database.main:run"]},
    install_requires=requirements,
)
