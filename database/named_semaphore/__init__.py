from gevent.lock import Semaphore


class NamedSemaphore:
    __semaphores = {}

    def __init__(self):
        pass

    def for_name(self, name):
        if name in self.__semaphores:
            return self.__semaphores[name]
        else:
            semaphore = Semaphore(1)
            self.__semaphores[name] = semaphore
            return semaphore

    def release_all(self):
        [s.release() for s in self.__semaphores.values()]


named_semaphore = NamedSemaphore()
