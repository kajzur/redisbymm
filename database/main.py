from lexical_libraries.redisbymmLexer import redisbymmLexer
from lexical_libraries.redisbymmParser import redisbymmParser
from lexical_libraries import RedisByMMListener, RedisByMMErrorListener
from antlr4 import *
from gevent.server import StreamServer
from gevent.monkey import patch_all

patch_all()


def run():
    def handle(socket, address):
        fp = socket.makefile()
        while True:

            try:
                line = fp.readline()
                line = str(line).rstrip()
            except:
                socket.send(b"Error in reading input.")
                socket.close()
                break

            if line == "QUIT":
                socket.close()
                break

            if line:
                lexer = redisbymmLexer(InputStream(line))
                stream = CommonTokenStream(lexer)
                parser = redisbymmParser(stream)

                parser.removeErrorListeners()
                parser.addErrorListener(RedisByMMErrorListener(socket))
                tree = parser.expression()
                listener = RedisByMMListener(socket)
                walker = ParseTreeWalker()
                walker.walk(listener, tree)
            else:
                continue

    server = StreamServer(("127.0.0.1", 1234), handle)
    server.serve_forever()


if __name__ == "__main__":
    run()
