from database.named_semaphore import named_semaphore
from json import dumps


RUNTIME_ERROR_PREFIX = "Runtime_Err: "


def raise_err(msg):
    return '\n' + RUNTIME_ERROR_PREFIX + msg


class SynchronizedAccessDict(dict):
    def __setitem__(self, k, v) -> None:
        named_semaphore.for_name(k).acquire()
        super().__setitem__(k, v)
        named_semaphore.for_name(k).release()

    def __getitem__(self, item):
        named_semaphore.for_name(item).acquire()
        try:
            return super().__getitem__(item)
        except KeyError:
            raise KeyError
        finally:
            named_semaphore.for_name(item).release()

    def __delitem__(self, v) -> None:
        named_semaphore.for_name(v).acquire()
        try:
            super().__delitem__(v)
        except KeyError:
            raise KeyError
        finally:
            named_semaphore.for_name(v).release()


class Database:
    __data = SynchronizedAccessDict()

    def __init__(self):
        pass

    def add_or_replace_string(self, key, val):
        self.__data[key] = val

    def add_to_dict(self, key, string_ind, val):
        if key in self.__data:
            self.__data[key][string_ind] = val
        else:
            self.__data[key] = {string_ind: val}

    def remove_from_dict(self, key, string_ind):
        try:
            d = self.__data[key]
            del d[string_ind]
        except KeyError:
            pass

    def get_from_dict(self, key, string_ind):
        try:
            return self.__data[key][string_ind]
        except KeyError:
            return raise_err("No such key!")

    def add_to_list(self, key, val):
        if key in self.__data:
            self.__data[key].append(val)
        else:
            self.__data[key] = [val]

    def get_from_list(self, key, index):
        if index.isdigit():
            return self.__data[key][int(index)]
        else:
            return raise_err("Invalid index (expexting int)")

    def get_all_from_list(self, key):
        return dumps(self.__data[key])

    def pop_from_list(self, key, index):
        return self.__data[key].pop(int(index))

    def remove_from_list(self, key, index):
        self.pop_from_list(key, index)

    def get_type(self, key):
        return str(type(self.__data[key]))

    def del_key(self, key):
        del self.__data[key]

    def get(self, key):
        try:
            return self.__data[key]
        except KeyError:
            return raise_err("No such key!")


database = Database()
