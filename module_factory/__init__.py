import sys
import module_factory.modules


class ModuleFactory:
    @staticmethod
    def create_for_token(token):
        class_name = str(token[0]).upper() + str(token[1:]).lower() + "Module"
        print(class_name)
        return getattr(sys.modules[__name__ + ".modules"], class_name)()
