from database import database
from abc import abstractmethod


class Module:
    def __init__(self):
        self.contexts = []
        self._initialize_definition()

    def run(self):
        assert len(self.contexts) > 0
        result = self._run()
        if result:
            return str.encode(result)
        else:
            return str.encode("")

    @abstractmethod
    def _run(self):
        raise NotImplementedError()

    def _initialize_definition(self):
        self.context = self.ClassContext

    def set_key(self, key):
        self.contexts.append(self.context(key=key))

    def set_value(self, value, create=False):
        if create:
            self.contexts.append(self.context(key=None, value=value))
            return
        self.contexts[-1].value = value

    def set_index(self, index):
        self.contexts[-1].index = index

    def set_string_index(self, string_index):
        self.contexts[-1].string_index = string_index


class GetModule(Module):
    class ClassContext:
        def __init__(self, key):
            self.key = key

    def _run(self):
        value = database.get(self.contexts[0].key)
        if isinstance(value, str):
            return value
        else:
            return database.raise_err(
                "Invalid type. Trying to get something that is not string."
            )


class SetvModule(Module):
    class ClassContext:
        def __init__(self, key, value=None):
            self.key = key
            self.value = value

    def _run(self):
        [database.add_or_replace_string(c.key, c.value) for c in self.contexts]


class SaddModule(Module):
    class ClassContext:
        def __init__(self, key, value=None, string_index=None):
            self.key = key
            self.value = value
            self.string_index = string_index

    def _run(self):
        ctx = self.contexts[0]
        database.add_to_dict(ctx.key, ctx.string_index, ctx.value)


class SdelModule(Module):
    class ClassContext:
        def __init__(self, key, string_index=None):
            self.key = key
            self.string_index = string_index

    def _run(self):
        ctx = self.contexts[0]
        database.remove_from_dict(ctx.key, ctx.string_index)


class SgetModule(Module):
    class ClassContext:
        def __init__(self, key, string_index=None):
            self.key = key
            self.string_index = string_index

    def _run(self):
        ctx = self.contexts[0]
        return database.get_from_dict(ctx.key, ctx.string_index)


class LaddModule(Module):
    class ClassContext:
        def __init__(self, key=None, value=None):
            self.key = key
            self.value = value

    def _run(self):
        key = self.contexts[0].key
        print("ladd: {}".format(len(self.contexts)))
        [database.add_to_list(key, c.value) for c in self.contexts[1:]]


class LdelModule(Module):
    class ClassContext:
        def __init__(self, key=None, index=None):
            self.key = key
            self.index = index

    def _run(self):
        key = self.contexts[0].key
        [database.remove_from_list(key, c.index) for c in self.contexts]


class LgetModule(Module):
    class ClassContext:
        def __init__(self, key=None, index=None):
            self.key = key
            self.index = index

    def _run(self):
        ctx = self.contexts[0]
        return database.get_from_list(ctx.key, ctx.index)


class LgetallModule(Module):
    class ClassContext:
        def __init__(self, key=None):
            self.key = key

    def _run(self):
        ctx = self.contexts[0]
        return database.get_all_from_list(ctx.key)


class LpopModule(Module):
    class ClassContext:
        def __init__(self, key, index=None):
            self.key = key
            self.index = index

    def _run(self):
        ctx = self.contexts[0]
        return database.pop_from_list(ctx.key, ctx.index)


class GettypeModule(Module):
    class ClassContext:
        def __init__(self, key):
            self.key = key

    def _run(self):
        ctx = self.contexts[0]
        return database.get_type(ctx.key)


class DelkModule(Module):
    class ClassContext:
        def __init__(self, key):
            self.key = key

    def _run(self):
        ctx = self.contexts[0]
        try:
            return database.del_key(ctx.key)
        except KeyError:
            return database.raise_err("Key not found.")
